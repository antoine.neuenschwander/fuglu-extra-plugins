# -*- coding: utf-8 -*-

import unittest
import logging
import sys
import multiprocessing
import threading
import time
from io import BytesIO
from os import getpid

from uriextract.uriextract import URIExtract, EmailExtract, DOMAINMAGIC_AVAILABLE
from testing.storedmails import mail_html, mail_base64
from fuglu.shared import Suspect
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header

try:
    import unittest.mock as mock
except ImportError:
    import mock

try:
    #py2
    import ConfigParser
except ImportError:
    #py3
    import configparser as ConfigParser

def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


if DOMAINMAGIC_AVAILABLE:
    class URIExtractTest(unittest.TestCase):
        def setUp(self):
            section="URIExtract"

            tlds="com net org\n .co.uk ch ru"
            open('/tmp/tld.txt','w').write(tlds)

            skiplist="skipme.com meetoo.com"
            open('/tmp/domainskiplist.txt','w').write(skiplist)


            config=ConfigParser.RawConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
            config.set(section, 'maxsize', 10485000)
            config.set(section, 'loguris', 'no')
            self.candidate=URIExtract(config,section)
            self.candidate._prepare()

        def tearDown(self):
            pass

        def test_simple_text(self):
            txt="""hello http://bla.com please click on <a href="www.co.uk">slashdot.org/?a=c&f=m</a> www.skipme.com www.skipmenot.com/ http://allinsurancematters.net/lurchwont/ muahahaha x.org"""

            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('http://bla.com' in uris)
            self.assertTrue('www.co.uk' in uris)
            self.assertTrue('slashdot.org/?a=c&f=m' in uris)

            self.assertTrue('www.skipmenot.com/' in uris)
            #print(" ".join(uris))
            self.assertTrue("skipme.com" not in " ".join(uris))

            self.assertTrue("http://allinsurancematters.net/lurchwont/" in uris)
            self.assertTrue("x.org" in uris,'rule at the end not found')

        def test_dotquad(self):
            txt="""click on 1.2.3.4 or http://62.2.17.61/ or https://8.8.8.8/bla.com """

            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('1.2.3.4' in uris)
            self.assertTrue('http://62.2.17.61/' in uris)
            self.assertTrue('https://8.8.8.8/bla.com' in uris)

        def test_uppercase(self):
            txt="""hello http://BLa.com please click"""
            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('http://bla.com' not in uris,'uris should not be lowercased')
            self.assertTrue('http://BLa.com' in uris,'uri with uppercase not found')

        def test_url_without_file(self):
            txt="""lol http://roasty.familyhealingassist.ru?coil&commission blubb"""
            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('http://roasty.familyhealingassist.ru?coil&commission' in uris,'did not find uri, result was %s'%uris)

        def test_withSuspect_TE(self):
            """Test using suspect, link is in the base64 transfer encoded part"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Read file content")
            filecontent = BytesIO(mail_base64).read()

            logger.debug("Create suspect")
            suspect = Suspect("auth@aaaaaa.aa","rec@aaaaaaa.aa","/dev/null")
            suspect.set_source(filecontent)

            logger.debug("examine suspect")
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            logger.debug('uris: '+",".join(uris))

            self.assertTrue('www.co.uk' in uris)

        def test_unquoted_link(self):
            """Test unquoted href attribute"""
            txt="""hello  <a href=www.co.uk>slashdot.org/?a=c&f=m</a> """

            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('www.co.uk' in uris)
            self.assertTrue('slashdot.org/?a=c&f=m' in uris)

        def test_withSuspect(self):
            """Test unquoted href attribute in html part of mail"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Read file content")
            filecontent = BytesIO(mail_html).read()

            logger.debug("Create suspect")
            suspect = Suspect("auth@aaaaaa.aa","rec@aaaaaaa.aa","/dev/null")
            suspect.set_source(filecontent)

            logger.debug("examine suspect")
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            logger.debug('uris: '+",".join(uris))
            self.assertTrue( "http://toBeDetected.com.br/Jul2018/En/Statement/Invoice-DDDDDDDDD-DDDDDD/" in uris)


        def test_withSuspect_newDecode(self):
            """Test if new version of URIExtract gives same result as old one"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Read file content")
            filecontent = BytesIO(mail_html).read()

            logger.debug("Create suspect")
            suspect = Suspect("auth@aaaaaa.aa","rec@aaaaaaa.aa","/dev/null")
            suspect.set_source(filecontent)

            textparts_deprecated = self.candidate.get_decoded_textparts_deprecated(suspect)
            textparts            = self.candidate.get_decoded_textparts(suspect,bcompatible=False)

            self.assertEqual(textparts_deprecated,textparts)

    class EmailExtractTest(unittest.TestCase):
        """Test email address extraction"""

        def setUp(self):
            section="EmailExtract"

            tlds="com net org\n .co.uk ch ru"
            open('/tmp/tld.txt','w').write(tlds)

            skiplist="skipme.com meetoo.com"
            open('/tmp/domainskiplist.txt','w').write(skiplist)


            config=ConfigParser.RawConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
            config.set(section, 'maxsize', 10485000)
            config.set(section, 'loguris', 'no')

            config.set(section, 'headers', 'Return-Path,Reply-To,From,X-RocketYMMF,X-Original-Sender,Sender,X-Originating-Email,Envelope-From,Disposition-Notification-To')
            config.set(section, 'skipheaders', 'X-Original-To,Delivered-To,X-Delivered-To,Apparently-To,X-Apparently-To')

            self.candidate=EmailExtract(config,section)
            self.candidate._prepare()

        def test_withSuspect(self):
            """Test email address extraction"""

            # for testing, collect all addresses that should be found
            address2befound = []

            # me == my email address
            # you == recipient's email address
            me = "my@tobefound.com"
            address2befound.append(me)

            you = "your@tobeskipped.com"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "mail address"
            msg['From'] = me
            msg['To'] = you

            # Create the body of the message (a plain-text and an HTML version).
            addr = "webmaster@findmeintext.com"
            address2befound.append(addr)
            text = "Hi!\nHow are you?\nHere is the link you wanted:\n" \
                   "https://www.python.org\nAnd here's the address:\n%s."%addr

            addr = "webmaster@findmeinhtml.com"
            html = u"""\                                                                                     
            <html>                                                                                           
              <head></head>                                                                                  
              <body>                                                                                         
                <p>Hi!<br>                                                                                   
                   How are you?<br>                                                                          
                   Here is the <a href="https://www.python.org">link</a> you wanted.<br>       
                   And here's the <a href="mailto:%s"> mail address</a>.<br>
                </p>                                                                                         
              </body>                                                                                        
            </html>                                                                                          
            """%addr
            address2befound.append(addr)

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html',_charset="UTF-8")

            msg.attach(part1)
            msg.attach(part2)

            headerlist = ['Return-Path', 'Reply-To', 'From', 'X-RocketYMMF', 'X-Original-Sender', 'Sender',
                          'X-Originating-Email', 'Envelope-From', 'Disposition-Notification-To']
            for hdr in headerlist:
                addr = hdr.lower()+"@tobefound.com"
                msg[hdr] = addr
                address2befound.append(addr)
                print("New/changed header to be found %s: %s"%(hdr,msg[hdr]))

            skipheaderlist = ['X-Original-To', 'Delivered-To', 'X-Delivered-To,' 'Apparently-To', 'X-Apparently-To']
            for hdr in skipheaderlist:
                msg[hdr] = hdr.lower()+"@tobeskipped.com"
                print("New/changed header to be skipped %s: %s"%(hdr,msg[hdr]))

            print("Create suspect")
            suspect = Suspect("auth@aaaaaa.aa","rec@aaaaaaa.aa","/dev/null")

            try:
                suspect.set_source(msg.as_bytes())
            except AttributeError:
                suspect.set_source(msg.as_string())

            print("Examine suspect")
            self.candidate.examine(suspect)

            emailaddresses = suspect.get_tag('emails')
            print('email addresses found: '+",".join(emailaddresses))

            missing = []
            for addr in address2befound:
                if addr in emailaddresses:
                    print("Found: %s"%addr)
                else:
                    print("DID NOT FIND: %s"%addr)
                    missing.append(addr)

            over = []
            for addr in emailaddresses:
                if addr not in address2befound:
                    print("DID FIND ADDRESS WHICH SHOULD NOT BE FOUND: %s"%addr)
                    over.append(addr)

            self.assertEqual(0,len(missing),"Not all mail addresses detected! Missing:\n[%s]\n\n"%", ".join(missing))
            self.assertEqual(0,len(over),"Found addresses that should be skipped! List is:\n[%s]\n\n"%", ".join(over))

        def test_withSuspect_Header_problem(self):
            """From bugfix where EmailExtract complained about a getting a Header instead of str in Py3"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Read file content")
            filecontent = BytesIO(mail_html).read()

            logger.debug("Create suspect")
            suspect = Suspect("auth@aaaaaa.aa","rec@aaaaaaa.aa","/dev/null")
            suspect.set_source(filecontent)
            msg = suspect.get_message_rep()
            msg['Reply-To'] = "rec@aaaaaaa.aa"                   # works
            msg['Reply-To'] = Header("rec@bbbbbbb.bb").encode()  # works
            msg['Reply-To'] = Header("rec@ccccccc.cc")           # (failing before bugfix)
            msg['Reply-To'] = Header("rec@ddddddd.dd")           # works
            suspect.set_message_rep(msg)

            # no exception is fine for this case
            self.candidate.examine(suspect)

            emailaddresses = suspect.get_tag('emails')
            print('email addresses found: '+",".join(emailaddresses))
            # rec@aaaaaaa.aa and rec@ddddddd.dd are ignored because ".aa" and ".dd" are no valid tlds
            self.assertEqual(sorted(["rec@bbbbbbb.bb", "rec@ccccccc.cc"]), sorted(emailaddresses))

    class URIExtractMultiproc(unittest.TestCase):
        """Some problems have been found in multiprocessing mode using the domainmagic module
        that's why it gets here a separate test"""
        def setUp(self):

            tlds="com net org\n .co.uk ch ru"
            open('/tmp/tld.txt','w').write(tlds)

            skiplist="skipme.com meetoo.com"
            open('/tmp/domainskiplist.txt', 'w').write(skiplist)

        def test_parallelrun(self):
            """Tests domainmagic for its multiprocessing capabilities which is required uding URIExtract
            in fuglu with multiprocessing backend"""

            import domainmagic

            # check version of domainmagic
            dversion = [int(i) for i in domainmagic.__version__.split(".")]
            dminversion = [0, 0, 10]
            newer = False
            equal = True
            for dv, dmv in zip(dversion, dminversion):
                newer = newer or (dv > dmv)
                equal = equal and (dv == dmv)

            if not (newer or equal):
                print("\n")
                print("-------------------------------------------------------------")
                print("-- This test can only run with domainmagic 0.0.10 or newer --")
                print("-------------------------------------------------------------")
                print("Version detected is: %s (%s)" % (domainmagic.__version__,".".join([str(i) for i in dversion])))
                print("\n")
                return

            #from domainmagic.fileupdate import fileupdater  <- won't work, will remain None
            import domainmagic.fileupdate


            root = logging.getLogger()
            root.setLevel(logging.ERROR)


            if domainmagic.fileupdate.fileupdater is not None:
                domainmagic.fileupdate.fileupdater.is_recent = mock.Mock(return_value=False)
            else:
                print("%u: no fileupdater object has been created yet" % getpid())

            parallel_tasks = 2
            resultqueue = multiprocessing.Queue()
            jobs = []

            # run worker once before creating processes
            run_worker(None, 0, -1)

            # get the id of the Fileupdater
            fileupdaterid = domainmagic.fileupdate.fileupdater.id()

            print("------------------")
            for i in range(parallel_tasks):
                workerid = i+1  # workerid starts with 1 so it can be distinguished from main thread which has 0
                p = multiprocessing.Process(target=run_worker, args=(resultqueue, workerid, fileupdaterid))
                jobs.append(p)

            for p in jobs:
                p.start()

            njobs = len(jobs)
            for p,i in zip(jobs,range(njobs)):
                # max timeout for the first job
                p.join(timeout=(njobs-i))
                print("Process %u is alive: %s" % (i, p.is_alive()))

            resultqueue.put(None)
            errorprocs = []
            while True:
                procanswer = resultqueue.get(1)
                if procanswer is None:
                    # this is the end of the queue
                    break
                pid, status = procanswer
                print("process %u, success %u" % (pid, status))
                if status != 0:
                    errorprocs.append(pid)

            self.assertEqual(0, len(errorprocs))

    def run_worker(resultqueue, workerid, fileupdaterid):

        #from domainmagic.fileupdate import fileupdater  <- won't work, will remain None, global var "fileupdater" has
        #                                                   to be accessed as domainmagic.fileupdate.fileupdater

        import domainmagic.fileupdate

        # a mock to force file update
        is_recent_mock = mock.Mock(return_value=False)

        if domainmagic.fileupdate.fileupdater is not None:
            if domainmagic.fileupdate.fileupdater.is_recent is not is_recent_mock:
                domainmagic.fileupdate.fileupdater.is_recent = is_recent_mock
            print("%u: (beginning of run_worker) fileupdate id: %u, instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), id(domainmagic.fileupdate.FileUpdaterMultiproc.instance)))
            print("%u: (beginning of run_worker) fileupdate id: %u, access -> instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), domainmagic.fileupdate.fileupdater.id()))
        else:
            print("%u: (beginning of run_worker) no fileupdater object has been created yet" % getpid())

        if workerid == 0:
            # In the beginning when the main thread reaches here
            # there should be no fileupdater yet
            if (fileupdaterid != -1):
                raise ValueError("Main process should be called only once with hardcoded '-1' as id")
            # Unfortunately during tests it can not be guaranteed another test using domainmagic
            # has run before and therefore domainmagic.fileupdate.fileupdater might not be None anymore

        section="EmailExtract"
        config=ConfigParser.RawConfigParser()
        config.add_section(section)
        config.set(section, 'tldfiles', "/tmp/tld.txt")
        config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
        config.set(section, 'maxsize', 10485000)
        config.set(section, 'loguris', 'no')

        config.set(section, 'headers', 'Return-Path,Reply-To,From,X-RocketYMMF,X-Original-Sender,Sender,X-Originating-Email,Envelope-From,Disposition-Notification-To')
        config.set(section, 'skipheaders', 'X-Original-To,Delivered-To,X-Delivered-To,Apparently-To,X-Apparently-To')

        candidate=URIExtract(config,section)
        candidate._prepare()
        candidate.extractor.maxregexage = 0

        if domainmagic.fileupdate.fileupdater is not None:
            if domainmagic.fileupdate.fileupdater.is_recent is not is_recent_mock:
                domainmagic.fileupdate.fileupdater.is_recent = is_recent_mock
            print("%u: (after preparing URIExtract) fileupdate id: %u, instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), id(domainmagic.fileupdate.FileUpdaterMultiproc.instance)))
            print("%u: (after preparing URIExtract) fileupdate id: %u, access -> instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), domainmagic.fileupdate.fileupdater.id()))
        else:
            print("%u: (after preparing URIExtract) no fileupdater object has been created yet" % getpid())

        if workerid != 0 and fileupdaterid == domainmagic.fileupdate.fileupdater.id():
            raise ValueError("Subprocesses should have a different fileupdeter-object (and therefore id)\n"
                             "than the main thread after using it.")

        # Keep playing with a local threading.Lock here
        # In the old implementation it was getting stuck trying to
        # acquire a lock (randomly happened on some os version only)
        lock = threading.Lock()

        for i in range(2):
            lock.acquire()
            time.sleep(0.01)
            lock.release()
            txt="""hello http://bla.com please click on <a href="www.co.uk">slashdot.org/?a=c&f=m</a> www.skipme.com www.skipmenot.com/ http://allinsurancematters.net/lurchwont/ muahahaha x.org"""
            uris=candidate.extractor.extracturis(txt)

        if domainmagic.fileupdate.fileupdater is not None:
            if domainmagic.fileupdate.fileupdater.is_recent is not is_recent_mock:
                domainmagic.fileupdate.fileupdater.is_recent = is_recent_mock
            print("%u: (after extracting uris) fileupdate id: %u, instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), id(domainmagic.fileupdate.FileUpdaterMultiproc.instance)))
            print("%u: (after extracting uris) fileupdate id: %u, access -> instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), domainmagic.fileupdate.fileupdater.id()))
        else:
            print("%u: (after extracting uris) no fileupdater object has been created yet" % getpid())

        procid = getpid()
        target_found_list = ['http://bla.com', 'www.co.uk', 'slashdot.org/?a=c&f=m']
        for targeturi, i in zip(target_found_list,range(len(target_found_list))):
            if targeturi not in uris:
                if resultqueue is not None:
                    resultqueue.put((procid, i+1))
                raise ValueError("process %u -> %s not found!" % (procid, targeturi))

        if resultqueue is not None:
            resultqueue.put((procid, 0))
